#include <iostream>
#include <iomanip>
using namespace std;
double hermite(int n, double x);
int main()  
{  
      int a;
      double b;
      double M;
      cin>>a>>b;
      M=hermite(a,b);
      cout<<M;
    return 0;  
}
double hermite(int n, double x)  
{  
    if(n<=0)  
    return 1;
    else if(n==1)  
    return 2*x; 
    else 
    return     2*x*hermite(n-1,x)-2*(n-1)*hermite(n-2,x);
} 

